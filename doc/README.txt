This file contains the credits for The Dark Module.

1. Models, Characters:

- Nifrek (GPL): Megaerebus, Erebus, Nyx, Ignis, Seraphina, Pyria, Umbra, Gak.
- Nifrek, MirceaKitsune (GPL): Terminus, Termina, Draconi, Draconia.

2. Textures:

- Evillair (GPL): ex, ex2
