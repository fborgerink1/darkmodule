// cyborgs, megaerebus

model troopers_megaerebus_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/megaerebus_head.md5mesh
}

model troopers_megaerebus_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/megaerebus_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_megaerebus_head
{
	"editor_usage"					"Megaerebus head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_megaerebus_head"
	"skin"							"troopers/erebus_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_megaerebus
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Megaerebus."
	"editor_displayFolder"			"AI/Troopers/Cyborgs"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_megaerebus_body"
	"skin"				 			"troopers/erebus_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_megaerebus_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_MALE"

	"health"						"100"
	"mass"							"120"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_cynic_citywatch"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"1"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_megaerebus
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Megaerebus."

	"model"							"troopers_megaerebus_body"
	"skin"							"troopers/erebus_body"
	"def_head"						"atdm:ai_troopers_megaerebus_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_MALE"
}

// cyborgs, erebus

model troopers_erebus_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/erebus_head.md5mesh
}

model troopers_erebus_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/erebus_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_erebus_head
{
	"editor_usage"					"Erebus head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_erebus_head"
	"skin"							"troopers/erebus_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_erebus
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Erebus."
	"editor_displayFolder"			"AI/Troopers/Cyborgs"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_erebus_body"
	"skin"				 			"troopers/erebus_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_erebus_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_MALE"

	"health"						"100"
	"mass"							"120"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_cynic_citywatch"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"1"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_erebus
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Erebus."

	"model"							"troopers_erebus_body"
	"skin"							"troopers/erebus_body"
	"def_head"						"atdm:ai_troopers_erebus_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_MALE"
}

// cyborgs, nyx

model troopers_nyx_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/nyx_head.md5mesh
}

model troopers_nyx_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/nyx_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_nyx_head
{
	"editor_usage"					"Nyx head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_nyx_head"
	"skin"							"troopers/erebus_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_nyx
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Nyx."
	"editor_displayFolder"			"AI/Troopers/Cyborgs"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_nyx_body"
	"skin"				 			"troopers/erebus_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_nyx_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_FEMALE"

	"health"						"100"
	"mass"							"120"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_wench_soft_guard_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"1"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_nyx
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Nyx."

	"model"							"troopers_nyx_body"
	"skin"							"troopers/erebus_body"
	"def_head"						"atdm:ai_troopers_nyx_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_FEMALE"
}

// cyborgs, terminus

model troopers_terminus_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/terminus_head.md5mesh
}

model troopers_terminus_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/terminus_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_terminus_head
{
	"editor_usage"					"Terminus head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_terminus_head"
	"skin"							"troopers/terminus_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_terminus
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Terminus."
	"editor_displayFolder"			"AI/Troopers/Cyborgs"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_terminus_body"
	"skin"				 			"troopers/terminus_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_terminus_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_MALE"

	"health"						"100"
	"mass"							"120"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_cynic_citywatch"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"1"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_terminus
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Terminus."

	"model"							"troopers_terminus_body"
	"skin"							"troopers/terminus_body"
	"def_head"						"atdm:ai_troopers_terminus_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_MALE"
}

// cyborgs, termina

model troopers_termina_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/termina_head.md5mesh
}

model troopers_termina_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/termina_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_termina_head
{
	"editor_usage"					"Termina head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_termina_head"
	"skin"							"troopers/termina_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_termina
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Termina."
	"editor_displayFolder"			"AI/Troopers/Cyborgs"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_termina_body"
	"skin"				 			"troopers/terminus_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_termina_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_FEMALE"

	"health"						"100"
	"mass"							"120"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_wench_soft_guard_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"1"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_termina
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Termina."

	"model"							"troopers_termina_body"
	"skin"							"troopers/terminus_body"
	"def_head"						"atdm:ai_troopers_termina_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_FEMALE"
}
