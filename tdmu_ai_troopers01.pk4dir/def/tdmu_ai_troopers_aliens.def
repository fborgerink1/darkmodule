// aliens, gak

model troopers_gak_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/gak_head.md5mesh
}

model troopers_gak_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/gak_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_gak_head
{
	"editor_usage"					"Gak head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_gak_head"
	"skin"							"troopers/gak_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_gak
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Gak."
	"editor_displayFolder"			"AI/Troopers/Aliens"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_gak_body"
	"skin"				 			"troopers/gak_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_gak_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_MALE"

	"health"						"100"
	"mass"							"110"

	"team"							"2"
	"rank"							"1"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_zombie_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"0"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_gak
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Gak."

	"model"							"troopers_gak_body"
	"skin"							"troopers/gak_body"
	"def_head"						"atdm:ai_troopers_gak_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_MALE"
}

// aliens, gak masked

model troopers_gak_masked_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/gak_masked_head.md5mesh
}

entityDef atdm:ai_troopers_gak_masked_head
{
	"editor_usage"					"Gak head, masked."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_gak_masked_head"
	"skin"							"troopers/gak_masked_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_gak_masked
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Gak, masked."
	"editor_displayFolder"			"AI/Troopers/Aliens"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_gak_body"
	"skin"				 			"troopers/gak_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_gak_masked_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_MALE"

	"health"						"100"
	"mass"							"110"

	"team"							"2"
	"rank"							"1"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_zombie_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"1"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_gak_masked
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Gak, masked."

	"model"							"troopers_gak_body"
	"skin"							"troopers/gak_body"
	"def_head"						"atdm:ai_troopers_gak_masked_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_MALE"
}

// aliens, draconi

model troopers_draconi_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/draconi_head.md5mesh
}

model troopers_draconi_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/draconi_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_draconi_head
{
	"editor_usage"					"Draconi head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_draconi_head"
	"skin"							"troopers/draconi_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_draconi
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Draconi."
	"editor_displayFolder"			"AI/Troopers/Aliens"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_draconi_body"
	"skin"				 			"troopers/draconi_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_draconi_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_MALE"

	"health"						"100"
	"mass"							"110"

	"team"							"2"
	"rank"							"1"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_zombie_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"0"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_draconi
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Draconi."

	"model"							"troopers_draconi_body"
	"skin"							"troopers/draconi_body"
	"def_head"						"atdm:ai_troopers_draconi_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_MALE"
}

// aliens, draconia

model troopers_draconia_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/draconia_head.md5mesh
}

model troopers_draconia_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/draconia_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_draconia_head
{
	"editor_usage"					"Draconia head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_draconia_head"
	"skin"							"troopers/draconi_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_draconia
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Draconia."
	"editor_displayFolder"			"AI/Troopers/Aliens"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_draconia_body"
	"skin"				 			"troopers/draconi_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_draconia_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_MALE"

	"health"						"100"
	"mass"							"110"

	"team"							"2"
	"rank"							"1"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_zombie_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"0"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_draconia
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Draconia."

	"model"							"troopers_draconia_body"
	"skin"							"troopers/draconi_body"
	"def_head"						"atdm:ai_troopers_draconia_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_MALE"
}
